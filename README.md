# Simple Biosphere Model Version 4.1

SiB4 has been updated to version 2, please see https://gitlab.com/kdhaynes/sib4v2_corral.  <br>
SiB4 version 1 code currently requires explicit permission to view and use; however, we can provide this to interested users upon request.

## Model Description

SiB4 is a land surface model that utilizes a **single framework** to predict:


- Land cover state
  - Land surface properties
  - Vegetation characteristics


* Land surface energy and water budgets
  * Land-atmosphere exchanges of energy, momentum, radiation, and water
  * Soil moisture content

- Terrestrial Carbon Cycle
  * Photosynthesis: Enzyme Kinetics and Stomatal Physiology
  * Respiration: Autotrophic and Heterotrophic
  * Carbon Pools: 5 live and 6 dead

SiB4 uses **prognostic phenology** to unify processes:
- Dynamic Stages (non-crops)
  - Leaf-out, growth, maturity, stress, dormant
* Defined Stages (crops)
  * Growing-degree-day or days after planting
- Removes vegetation-state dependency
  * Inputs: weather, plant type, and soil type

SiB4 predicts sub-hourly exchanges/fluxes and daily carbon pools.

## Features
- Vegetation: Plant Functional Types (PFTs)
- Land Cover Heterogeneity: Tiles
- Carbon Pools: Mechanistic respiration and transfer methods
- Prognostic Phenology: Dynamic (non-crop) and defined (crop) stages
- Disturbance: Grassland Grazing and Crop Harvest
- Terrestrial Carbon Cycle: Unified fluxes, pools, phenology, and disturbance
- Emergent Properties:
   * Time: 10-minute time-step 
     - Hourly, daily, weekly, seasonal, and decadal predictions
     - Responds to weather and environmental conditions 
     - Capable of predicting climatic anomalies, such as response to long-term drought
   * Space: 0.5-degree resolution 
      - Spatial gradients in response to climate on local, regional, and global scales
- I/O:
   * Removed dependence on satellite vegetation state
   * Wide variety of evaluation metrics

## References
- SiB4 version 1 is described in [Haynes et al. 2019a](https://doi.org/10.1029/2018MS001540)
- SiB4 version 1 is evaluated at 55 grassland sites in [Haynes et al. 2019b](https://doi.org/10.1029/2018MS001541)
- SiB4 is modified from SiB3 ([Baker et al. 2008](https://doi.org/10.1029/2007JG000644); [Sellers et al., 1996](https://doi.org/10.1175/1520-0442(1996)009%3C0676:ARLSPF%3E2.0.CO;2))
   - Switched from biomes to plant functional types (PFTs)
   - Added tiles for fractional coverage per grid cell
   - Modified the variable structure to hierarchy of grid cells, land units, and PFTs

* Incorporated carbon pools following basic SiB-CASA scheme ([Schaefer et al., 2008](https://doi.org/10.1029/2007JG000603))
  * 6 live pools (storage, leaf, fine root, coarse root, wood, product)
  * 6 dead pools (coarse woody debris, metabolic litter, structural litter, soil litter, soil slow, soil armored)

- Utilized prognostic phenology growing season index concept ([St&ouml;ckli et al. 2011](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2010JG001545); [St&ouml;ckli et al. 2008](https://doi.org/10.1029/2008JG000781))
  - Predicts consistent carbon fluxes, pools, and leaf area index (LAI)
  - Calculates sub-hourly fluxes and updates phenology/pools daily

* Included crop specific phenology modules ([Lokupitiya et al. 2009](https://doi.org/10.5194/bg-6-969-2009); [Corbin et al. 2010](https://doi.org/10.1111/j.1600-0889.2010.00485.x))
  * Maize, Soybeans, and Wheat


## Funding
SiB4 was developed with NASA funding from the following projects:
- Carbon Cycle Science: NNX14A152G
- Carbon Monitoring System: NNX12AP86G
- Science Team for the OCO-2 Missions: NNX15AG93G
- Terrestrial Ecology: NNX11AB87G

## Contact
Katherine Haynes  
katherine.haynes.csu@gmail.com